# 用于学习测试BitMap的实现

## 碰撞测试

- package: collision

用于测试util包下几个CRC64实现的碰撞情况

```shell
java -cp bitmap17-1.0-SNAPSHOT.jar org.example.collision.CollisionMain
```

## 生成测试文件
- package: writebitfile


## 读取测试文件比对
- package: readbitfile


