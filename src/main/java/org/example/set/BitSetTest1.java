package org.example.set;

import java.util.BitSet;

public class BitSetTest1 {

    public static void main(String[] args) {
        // 使用无参构造器，还有2个有参构造器使用不多
        BitSet bitSet = new BitSet();
        // 把bit位2位置设置为1
        bitSet.set(2);
        System.out.println(bitSet.length());

        // 把3-5设置为1，[) 左闭右开
/*
        bitSet.set(3, 5);
        System.out.println(bitSet.length());
        // 把bit位2设置位0
        bitSet.set(2, false);
        System.out.println(bitSet.length());
        // 把3-5设置为0，[) 左闭右开
        bitSet.set(3, 5, false);
        System.out.println(bitSet.length());
*/

        bitSet.set(435704073);
        System.out.println(bitSet.length());
//        bitSet.set(4237244439);
//        System.out.println(bitSet.length());
    }

}
