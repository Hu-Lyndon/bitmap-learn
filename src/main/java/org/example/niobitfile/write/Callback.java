package org.example.niobitfile.write;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/26 13:07
 */
public interface Callback {
    void action(String line);
}
