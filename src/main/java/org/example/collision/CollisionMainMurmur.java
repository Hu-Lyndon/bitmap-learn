package org.example.collision;

import org.apache.commons.codec.digest.DigestUtils;
import org.example.util.MurmurHashUtil;
import org.roaringbitmap.longlong.LongUtils;
import org.roaringbitmap.longlong.Roaring64Bitmap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/23 10:39
 */
public class CollisionMainMurmur {

    private static final Logger LOG = LoggerFactory.getLogger(CollisionMainMurmur.class);
    public static void main(String[] args) {
        Random rand = new Random();
        Roaring64Bitmap roaring64Bitmap1 = new Roaring64Bitmap();

        for (int i = 0; i < 1000000000; i++) {
            if (i % 1000000 == 0) {
                System.out.println("row = " + i);
                LOG.info("row = " + i);
            }
            //String seed = "aaaaa" + i + System.currentTimeMillis()/1000 + rand.nextLong();
            String seed = "aaaaa" + i + System.currentTimeMillis()/1000 + rand.nextLong();
            String s = DigestUtils.md5Hex(seed.getBytes());
            //long crc32Id = CRC32Util.crc32Id(s);
            long l = MurmurHashUtil.getLongFromString(s);
            boolean generateIdContains = roaring64Bitmap1.contains(l);
            if (generateIdContains) {
                System.out.println("Murmur 重复:" + l);
                LOG.info("Murmur 重复:" + l);
            }
            roaring64Bitmap1.add(l);
        }
    }

}
