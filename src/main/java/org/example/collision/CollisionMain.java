package org.example.collision;

import org.apache.commons.codec.digest.DigestUtils;
import org.example.util.CRC64;
import org.example.util.CRC64Util;
import org.example.util.GenerateUtil;
import org.roaringbitmap.longlong.Roaring64Bitmap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/23 10:39
 */
public class CollisionMain {

    private static final Logger LOG = LoggerFactory.getLogger(CollisionMain.class);
    public static void main(String[] args) {
        Random rand = new Random();
        Roaring64Bitmap roaring64Bitmap1 = new Roaring64Bitmap();
        Roaring64Bitmap roaring64Bitmap2 = new Roaring64Bitmap();
        Roaring64Bitmap roaring64Bitmap3 = new Roaring64Bitmap();

        for (int i = 0; i < 100000000; i++) {
            if (i % 1000000 == 0) {
                System.out.println("row = " + i);
                LOG.info("row = " + i);
            }
            String seed = "aaaaa" + i + System.currentTimeMillis()/1000 + rand.nextLong();
            String s = DigestUtils.md5Hex(seed.getBytes());
            //Long generateId = GenerateUtil.getHash(s);
            long xiaobaoCrc64Id = CRC64.getId(s);
            //long crc64utilId = CRC64Util.crc64Long(s);
            /*boolean generateIdContains = roaring64Bitmap1.contains(generateId);
            if (generateIdContains) {
                System.out.println("Generate 重复:" + generateId);
                LOG.info("Generate 重复:" + generateId);
            }*/

            boolean xiaobaoCrc64IdContains = roaring64Bitmap2.contains(xiaobaoCrc64Id);
            if (xiaobaoCrc64IdContains) {
                System.out.println("xiaobaoCrc64 重复:" + xiaobaoCrc64Id);
                LOG.info("xiaobaoCrc64 重复:" + xiaobaoCrc64Id);
            }

            /*boolean crc64utilIdContains = roaring64Bitmap3.contains(crc64utilId);
            if (crc64utilIdContains) {
                System.out.println("crc64util 重复:" + crc64utilId);
                LOG.info("crc64util 重复:" + crc64utilId);
            }*/

            //roaring64Bitmap1.add(generateId);
            roaring64Bitmap2.add(xiaobaoCrc64Id);
            //roaring64Bitmap3.add(crc64utilId);
        }
    }

}
