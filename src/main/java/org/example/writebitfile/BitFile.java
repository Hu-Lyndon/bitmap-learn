package org.example.writebitfile;

import org.example.util.CRC64Util;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.zip.CRC32;

public class BitFile {

    public static void main(String[] args) {
        //List<String> data = Arrays.asList("aaaaa", "bbbbb");

        //FileInputStream fis = null;
        FileOutputStream fos = null;
        //DataInputStream dis = null;
        DataOutputStream dos = null;
        try {
            //创建输入流
//            fis = new FileInputStream("/lyndon/iProject/javapath/bitmap17/bitmap17/data/out/crc32file.txt");
//            dis = new DataInputStream(fis);
            //创建输出流
            //fos = new FileOutputStream("/lyndon/iProject/javapath/bitmap17/bitmap17/data/out/crc32file.txt");
            //fos = new FileOutputStream("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
            fos = new FileOutputStream("D:\\iProject\\javapath\\bitmap17\\data\\small\\crc32file.txt");
            dos = new DataOutputStream(fos);
            //循环读取录入
            /*int temp;
            while ((temp = dis.read()) != -1) {
                dos.write(temp);
            }*/

            //for (String datum : data) {
            for (int i = 0; i < 200; i++) {
                String datum = "aaaaa" + i;
                //Long crcId = crc32Id(datum);
                long crcId = CRC64Util.crc64Long(datum);
                //System.out.println("crcId:" + crcId);
                dos.writeLong(crcId);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                /*if (fis != null) {
                    fis.close();
                }*/
                if (fos != null) {
                    fos.close();
                }
                /*if (dis != null) {
                    dis.close();
                }*/
                if (dos != null) {
                    dos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static Long crc32Id(Object... args) {
        CRC32 crc32 = new CRC32();
        StringBuilder builder = new StringBuilder();
        for (Object arg : args) {
            if (arg == null) {
                builder.append("");
                continue;
            }
            builder.append(arg.toString().toUpperCase());
        }
        String seed = builder.toString();
        crc32.update(seed.getBytes(StandardCharsets.UTF_8));
        return crc32.getValue();
    }


}
