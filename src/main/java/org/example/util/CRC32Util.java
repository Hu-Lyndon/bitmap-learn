package org.example.util;

import java.nio.charset.StandardCharsets;
import java.util.zip.CRC32;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 9:39
 */
public class CRC32Util {

    //private static CRC32 crc32 = new CRC32();

    public static long crc32Id(Object... args) {
        CRC32 crc32 = new CRC32();
        StringBuilder builder = new StringBuilder();
        for (Object arg : args) {
            if (arg == null) {
                builder.append("");
                continue;
            }
            builder.append(arg.toString().toUpperCase());
        }
        String seed = builder.toString();
        crc32.update(seed.getBytes(StandardCharsets.UTF_8));
        return crc32.getValue();
    }


}
