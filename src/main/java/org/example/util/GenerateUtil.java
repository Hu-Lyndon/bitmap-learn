package org.example.util;

import java.nio.charset.StandardCharsets;
import java.util.zip.Adler32;
import java.util.zip.CRC32;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/23 10:13
 */
public class GenerateUtil {
    public static Long getHash(Object... args) {
        CRC32 crc32 = new CRC32();
        Adler32 adl32 = new Adler32();
        StringBuilder builder = new StringBuilder();
        for (Object arg : args) {
            if (arg == null) {
                builder.append("");
                continue;
            }
            builder.append(arg.toString().toUpperCase());
        }
        String seed = builder.toString();
        crc32.update(seed.getBytes(StandardCharsets.UTF_8));
        adl32.update(seed.getBytes(StandardCharsets.UTF_8));
        long crc = crc32.getValue();
        long adl = adl32.getValue();
        return (crc << 32) | adl;
    }

}
