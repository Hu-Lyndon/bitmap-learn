package org.example.util;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/23 10:06
 */
public class CRC64 {

    private static final long[] crcTable = new long[256];

    static {
        final long POLY = 0xC96C5795D7870F42L; // CRC64_ECMA_182 polynomial
        for (int b = 0; b < 256; b++) {
            long r = b;
            for (int i = 0; i < 8; i++) {
                if ((r & 1) != 0) {
                    r = (r >>> 1) ^ POLY;
                } else {
                    r >>>= 1;
                }
            }
            crcTable[b] = r;
        }
    }

    private long crc;

    public void update(byte[] bytes) {
        for (byte b : bytes) {
            crc = (crc >>> 8) ^ crcTable[(int) ((crc ^ b) & 0xFF)];
        }
    }

    public long getValue() {
        return crc;
    }

    public static long getId(String input) {
        CRC64 crc64 = new CRC64();
        byte[] data = input.getBytes();
        crc64.update(data);
        return crc64.getValue();
    }

}
