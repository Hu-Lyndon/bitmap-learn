package org.example.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2Utils;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 13:22
 */
public class Bz2Util {

    /**
     * 压缩生成bz2
     * @param file 源文件
     * @param targetPath 压缩之后生成bz2地址
     * @param delete 压缩后是否删除源文件
     */
    public static void compressToBz2(File file, String targetPath, boolean delete) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        BZip2CompressorOutputStream bz2Fos = null;
        try {
            fis = new FileInputStream(file);
            fos = new FileOutputStream(new File(targetPath));
            bz2Fos = new BZip2CompressorOutputStream(fos);
            int count;
            byte data[] = new byte[2048];
            while ((count = fis.read(data)) != -1) {
                bz2Fos.write(data, 0, count);
            }
            bz2Fos.flush();
            if(delete && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != bz2Fos) bz2Fos.close();
                if(null != fos) fos.close();
                if(null != fis) fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 解压缩bz2
     * @param file 压缩文件
     * @param targetPath 目标path
     * @param delete 解压后是否删除源文件
     */
    public static void uncompressFromBz2(File file, String targetPath, boolean delete) {
        FileInputStream fis = null;
        BZip2CompressorInputStream bz2Is = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(file);
            bz2Is = new BZip2CompressorInputStream(fis);
            fos = new FileOutputStream(new File(targetPath));
            int count;
            byte data[] = new byte[2048];
            while ((count = bz2Is.read(data)) != -1) {
                fos.write(data, 0, count);
            }
            fos.flush();
            if(delete && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != fis) fis.close();
                if(null != bz2Is) bz2Is.close();
                if(null != fos) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        //压缩bz2
        String srcFilePath = "D:/bz2-test/cms.txt";
        File srcFile = new File(srcFilePath);
        String fileName = BZip2Utils.getCompressedFilename(srcFile.getName());
        String targetPath = srcFile.getParent() + File.separator + fileName;
        compressToBz2(srcFile, targetPath, true);

        //解压缩bz2
        srcFilePath = targetPath;
        srcFile = new File(srcFilePath);
        fileName = BZip2Utils.getUncompressedFilename(srcFile.getName());
        targetPath = srcFile.getParent() + File.separator + fileName;
        uncompressFromBz2(srcFile, targetPath, true);
    }
}