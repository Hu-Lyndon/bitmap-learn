package org.example.readbitfile;

import org.roaringbitmap.buffer.BufferFastAggregation;
import org.roaringbitmap.buffer.ImmutableRoaringBitmap;
import org.roaringbitmap.buffer.MutableRoaringBitmap;
import org.roaringbitmap.longlong.Roaring64Bitmap;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;

public class ReadBit64RMCompare {
    
    public static void main(String[] args) {


        Roaring64Bitmap r64b1 = new Roaring64Bitmap();
        Roaring64Bitmap r64b2 = new Roaring64Bitmap();

        CountDownLatch latch = new CountDownLatch(2);

        Thread t1 = new Thread(() -> {
            long start = System.currentTimeMillis();
            String file = "D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt";
            readFile(file, r64b1);
            r64b1.runOptimize();
            latch.countDown();
            System.out.println("t1 load 耗时：" + (System.currentTimeMillis() - start));
        });


        Thread t2 = new Thread(() -> {
            long start = System.currentTimeMillis();
            String file = "D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file1.txt";
            readFile(file, r64b2);
            r64b2.runOptimize();
            latch.countDown();
            System.out.println("t2 load 耗时：" + (System.currentTimeMillis() - start));
        });
        
        t1.start();
        t2.start();

        try {
            latch.await();
        } catch (InterruptedException e) {
            System.err.println("InterruptedException: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Exception: " + e.getMessage());
        }

        long start = System.currentTimeMillis();
        diff(r64b1, r64b2);
        System.out.println("diff a - b 耗时：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        diff(r64b2, r64b1);
        System.out.println("diff b - a 耗时：" + (System.currentTimeMillis() - start));
    }


    private static void diff(Roaring64Bitmap a, Roaring64Bitmap b) {
        ByteBuffer outbb = ByteBuffer.allocate(Short.MAX_VALUE);
        try {
            a.serialize(outbb);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        outbb.flip();
        ImmutableRoaringBitmap irb = new ImmutableRoaringBitmap(outbb);

        ByteBuffer outbbr = ByteBuffer.allocate(Short.MAX_VALUE);
        try {
            b.serialize(outbbr);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        outbbr.flip();
        ImmutableRoaringBitmap irbr = new ImmutableRoaringBitmap(outbbr);
        MutableRoaringBitmap xor = BufferFastAggregation.xor(irb, irbr);
        MutableRoaringBitmap and = BufferFastAggregation.and(xor, irb);
        System.out.println(and);
    }

    /*private static void diff(Roaring64Bitmap a, Roaring64Bitmap b) {
        Roaring64Bitmap cloned = a.clone();
        cloned.xor(b);
        cloned.and(a);
        System.out.println(cloned);
    }*/


    public static void readFile(String fileFullPath, Roaring64Bitmap r64b) {
        FileInputStream fis = null;
//        FileOutputStream fos = null;
        DataInputStream dis = null;
//        DataOutputStream dos = null;
        try {
            //创建输入流
            //fis = new FileInputStream("/lyndon/iProject/javapath/bitmap17/bitmap17/data/out/crc32file.txt");
//            fis = new FileInputStream("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
            fis = new FileInputStream(fileFullPath);
            dis = new DataInputStream(fis);
            //循环读取录入
            long temp;
            long count = 0;
            while (dis.available() > 0) {
                try {
                    temp = dis.readLong();
                    //System.out.println("temp:" + temp);
                    compose(temp, r64b);
                    count ++;
                } catch (EOFException e) {
                    System.out.println("文件读取完毕");
                } catch (IOException e) {
                    System.err.println("文件读取异常:" + e.getMessage());
                }
            }
            System.out.println("count:" + count);
            System.out.println(r64b.getLongSizeInBytes());
            System.out.println(r64b.getSizeInBytes());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                /*if (fos != null) {
                    fos.close();
                }*/
                if (dis != null) {
                    dis.close();
                }
                /*if (dos != null) {
                    dos.close();
                }*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void compose(long crc64Id, Roaring64Bitmap r64b) {
        r64b.add(crc64Id);
    }
}
