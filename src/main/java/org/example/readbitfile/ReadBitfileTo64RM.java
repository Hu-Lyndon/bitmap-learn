package org.example.readbitfile;

import org.roaringbitmap.RoaringBitmap;
import org.roaringbitmap.longlong.Roaring64Bitmap;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ReadBitfileTo64RM {

    private static Roaring64Bitmap r64b = new Roaring64Bitmap();

    public static void main(String[] args) {
        //List<String> data = Arrays.asList("aaaaa", "bbbbb");

        FileInputStream fis = null;
//        FileOutputStream fos = null;
        DataInputStream dis = null;
//        DataOutputStream dos = null;
        try {
            //创建输入流
            //fis = new FileInputStream("/lyndon/iProject/javapath/bitmap17/bitmap17/data/out/crc32file.txt");
            fis = new FileInputStream("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
            dis = new DataInputStream(fis);
            //循环读取录入
            long temp;
            long count = 0;
            while (dis.available() > 0) {
                try {
                    temp = dis.readLong();
                    //System.out.println("temp:" + temp);
                    compose(temp);
                    count ++;
                } catch (EOFException e) {
                    System.out.println("文件读取完毕");
                } catch (IOException e) {
                    System.err.println("文件读取异常:" + e.getMessage());
                }
            }
            System.out.println("count:" + count);
            System.out.println(r64b.getLongSizeInBytes());
            System.out.println(r64b.getSizeInBytes());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                /*if (fos != null) {
                    fos.close();
                }*/
                if (dis != null) {
                    dis.close();
                }
                /*if (dos != null) {
                    dos.close();
                }*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void compose(long crc64Id) {
        r64b.add(crc64Id);
    }
}
