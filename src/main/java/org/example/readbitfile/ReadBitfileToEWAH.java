package org.example.readbitfile;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.roaringbitmap.RoaringBitmap;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class ReadBitfileToEWAH {

    // Positive 正
    public static ConcurrentHashMap<Long, EWAHCompressedBitmap> positiveBitmapConcurrentHashMap = new ConcurrentHashMap<>();
    // Negative 负
    public static ConcurrentHashMap<Long, EWAHCompressedBitmap> negativeBitmapConcurrentHashMap = new ConcurrentHashMap<>();

    public static void main(String[] args) {
        //List<String> data = Arrays.asList("aaaaa", "bbbbb");

        FileInputStream fis = null;
//        FileOutputStream fos = null;
        DataInputStream dis = null;
//        DataOutputStream dos = null;
        try {
            //创建输入流
            //fis = new FileInputStream("/lyndon/iProject/javapath/bitmap17/bitmap17/data/out/crc32file.txt");
            fis = new FileInputStream("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
            dis = new DataInputStream(fis);
            //循环读取录入
            long temp;
            long count = 0;
            while (dis.available() > 0) {
                try {
                    temp = dis.readLong();
                    //System.out.println("temp:" + temp);
                    compose(temp);
                    count ++;
                } catch (EOFException e) {
                    System.out.println("文件读取完毕");
                } catch (IOException e) {
                    System.err.println("文件读取异常:" + e.getMessage());
                }
            }

            System.out.println("count:" + count);
            System.out.println(negativeBitmapConcurrentHashMap.size());
            System.out.println(positiveBitmapConcurrentHashMap.size());


            AtomicLong totalPositiveSize = new AtomicLong();
            AtomicLong totalNegativeSize = new AtomicLong();

            negativeBitmapConcurrentHashMap.forEach((k, v) -> {
                totalNegativeSize.addAndGet(v.sizeInBits());
            });
            positiveBitmapConcurrentHashMap.forEach((k, v) -> {
                totalPositiveSize.addAndGet(v.sizeInBits());
            });

            System.out.println("totalPositiveSize:" + totalPositiveSize);
            System.out.println("totalNegativeSize:" + totalNegativeSize);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                /*if (fos != null) {
                    fos.close();
                }*/
                if (dis != null) {
                    dis.close();
                }
                /*if (dos != null) {
                    dos.close();
                }*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void compose(long crc64Id) {
        long num = crc64Id / Integer.MAX_VALUE;
        long pos = crc64Id % Integer.MAX_VALUE;
        //System.out.println(crc64Id + " -> " + num + " -> " + pos);

        if (crc64Id > 0) {
            // Positive 正
            EWAHCompressedBitmap rbMapOrDefault = positiveBitmapConcurrentHashMap.getOrDefault(num, new EWAHCompressedBitmap());
            rbMapOrDefault.set((int) pos);
            positiveBitmapConcurrentHashMap.put(num, rbMapOrDefault);
        } else {
            // Negative 负
            EWAHCompressedBitmap rbMapOrDefault = negativeBitmapConcurrentHashMap.getOrDefault(Math.abs(num), new EWAHCompressedBitmap());
            rbMapOrDefault.set((int) Math.abs(pos));
            negativeBitmapConcurrentHashMap.put(Math.abs(num), rbMapOrDefault);
        }
    }
}
