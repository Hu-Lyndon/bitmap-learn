package org.example.test;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.example.util.CRC32Util;
import org.example.util.CRC64Util;
import org.junit.Test;
import org.roaringbitmap.RoaringBitmap;
import org.roaringbitmap.longlong.Roaring64Bitmap;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 10:04
 */
public class RoaringbitmapTest {

    @Test
    public void testRoaringbitmap() {

        //向rr中添加1、2、3、1000四个数字
        RoaringBitmap rr = RoaringBitmap.bitmapOf(1, 2, 3, 1000);
        //创建RoaringBitmap rr2
        RoaringBitmap rr2 = new RoaringBitmap();
        //向rr2中添加10000-12000共2000个数字
//        rr2.add(10000L,12000L);
        //返回第3个数字是1000，第0个数字是1，第1个数字是2，则第3个数字是1000
        rr.add(Integer.MAX_VALUE);
        rr.select(3);
        //返回value = 2 时的索引为 1。value = 1 时，索引是 0 ，value=3的索引为2
        rr.rank(2);
        //判断是否包含1000
        rr.contains(1000); // will return true
        //判断是否包含7
        rr.contains(7); // will return false

        //两个RoaringBitmap进行or操作，数值进行合并，合并后产生新的RoaringBitmap叫rror
        RoaringBitmap rror = RoaringBitmap.or(rr, rr2);
        //rr与rr2进行位运算，并将值赋值给rr
//        rr.or(rr2);
        //判断rror与rr是否相等，显然是相等的
        boolean equals = rror.equals(rr);
        if (!equals) throw new RuntimeException("bug");
        // 查看rr中存储了多少个值，1,2,3,1000和10000-12000，共2004个数字
        long cardinality = rr.getLongCardinality();
//        System.out.println(cardinality);
        //遍历rr中的value
        for (int i : rr) {
            System.out.println(i);
        }
        //这种方式的遍历比上面的方式更快
        rr.forEach((Consumer<? super Integer>) i -> {
            System.out.println(i.intValue());
        });

    }

    @Test
    public void testRange() {

        for (long i = 0; i < 5; i++) {
            System.out.println(i * Integer.MAX_VALUE);
        }

        System.out.println("======================");

        // 4237244439
        long num = 4237244439L / Integer.MAX_VALUE;
        System.out.println(num);
        long pos = 4237244439L % Integer.MAX_VALUE;
        System.out.println(pos);

        System.out.println("----------------------");

        long data = pos + num * Integer.MAX_VALUE;
        System.out.println(data);


    }

    @Test
    public void testCase() {
        // [435704073, 4237244439]
        List<String> data1 = Arrays.asList("aaaaa", "bbbbb");

        data1.forEach(e -> {
            long l = CRC32Util.crc32Id(e);
            System.out.println(l);
        });

        System.out.println("=================================");


        int CONTAINER_MAX = 31250000;
        RoaringBitmap rb1 = new RoaringBitmap();

        /*int[] data = new int[1];
        data[0] = 435704073;

        rb1.addN(data,  data[0] % 64, 1);*/
        rb1.add(435704073);

        System.out.println("output RoaringBitmap:");
        rb1.forEach((Consumer<? super Integer>) i -> {
            System.out.println(i.intValue());
        });

        System.out.println("output RoaringBitmap long cardinality:" + rb1.getLongCardinality()
                + "\n cardinality:" + rb1.getCardinality());


    }

    @Test
    public void testCase1() {
        List<String> data1 = Arrays.asList("aaaaa", "bbbbb");

        RoaringBitmap rb1 = new RoaringBitmap();

        for (String datum1 : data1) {
            long crcId = CRC32Util.crc32Id(datum1);
            System.out.println("crcId:" + crcId);
            //rb1.add(crcId, crcId);
            rb1.add((int) crcId);

//            System.out.println(bm1.get(crcId));
        }

        // 0xffffffffL MAX_VALUE = 0x7fffffff;
        //Integer.MAX_VALUE
        // Long.MAX_VALUE;  0x7fffffffffffffffL

        rb1.forEach((Consumer<? super Integer>) i -> {
            System.out.println(i.intValue());
        });

    }


    @Test
    public void testSplitRangeCase1() {

        // Positive 正
        ConcurrentHashMap<Long, RoaringBitmap> positiveBitmapConcurrentHashMap = new ConcurrentHashMap<>();
        // Negative 负
        ConcurrentHashMap<Long, RoaringBitmap> negativeBitmapConcurrentHashMap = new ConcurrentHashMap<>();

        List<String> data = Arrays.asList("aaaaa", "bbbbb", "ccccc", "ddddd");
        data.forEach(e -> {
            long crc64Id = CRC64Util.crc64Long(e);
            long num = crc64Id / Integer.MAX_VALUE;
            long pos = crc64Id % Integer.MAX_VALUE;
            System.out.println(crc64Id + " -> " + num + " -> " + pos);

            if (crc64Id > 0) {
                // Positive 正
                RoaringBitmap rbMapOrDefault = positiveBitmapConcurrentHashMap.getOrDefault(num, new RoaringBitmap());
                rbMapOrDefault.add((int) pos);
                positiveBitmapConcurrentHashMap.put(num, rbMapOrDefault);
            } else {
                // Negative 负
                RoaringBitmap rbMapOrDefault = negativeBitmapConcurrentHashMap.getOrDefault(num, new RoaringBitmap());
                rbMapOrDefault.add((int) Math.abs(pos));
                negativeBitmapConcurrentHashMap.put(Math.abs(num), rbMapOrDefault);
            }

        });

        // Positive 正
        System.out.println("Positive 正");
        positiveBitmapConcurrentHashMap.forEach((k, v) -> {
            System.out.println(k + " -> " + v);
            System.out.println("-----------------");
            System.out.println("LongSize: " + v.getLongSizeInBytes() + " SizeInBytes:" + v.getSizeInBytes());
            System.out.println("=================");
        });

        // Negative 负
        System.out.println("Negative 负");
        negativeBitmapConcurrentHashMap.forEach((k, v) -> {
            System.out.print(k * -1 + " -> " );
            v.forEach((Consumer<? super Integer>) e -> System.out.println(e * -1));
            System.out.println("-----------------");
            System.out.println("LongSize: " + v.getLongSizeInBytes() + " SizeInBytes:" + v.getSizeInBytes());
            System.out.println("=================");
        });
    }

    @Test
    public void testRoaring64Bitmap0() {
        Roaring64Bitmap rbMapOrDefault1 = new Roaring64Bitmap();
        List<String> data = Arrays.asList("aaaaa", "bbbbb", "ccccc", "ddddd");
        data.forEach(e -> {
            long crc64Id = CRC64Util.crc64Long(e);
            rbMapOrDefault1.addLong(crc64Id);

        });
        System.out.println(rbMapOrDefault1);

        Roaring64Bitmap rbMapOrDefault2 = new Roaring64Bitmap();
        List<String> data2 = Arrays.asList("aaaaa", "bbbbb", "ccccc", "dee");
        data2.forEach(e -> {
            long crc64Id = CRC64Util.crc64Long(e);
            rbMapOrDefault2.addLong(crc64Id);

        });
        System.out.println(rbMapOrDefault2);

        Roaring64Bitmap cloned = rbMapOrDefault1.clone();
        cloned.xor(rbMapOrDefault2);
        cloned.and(rbMapOrDefault1);
        System.out.println(cloned);

        Roaring64Bitmap cloned2 = rbMapOrDefault2.clone();
        cloned2.xor(rbMapOrDefault1);
        cloned2.and(rbMapOrDefault2);
        System.out.println(cloned2);

    }

    @Test
    public void testRoaring64Bitmap() {
        // Positive 正
        ConcurrentHashMap<Long, Roaring64Bitmap> positiveBitmapConcurrentHashMap = new ConcurrentHashMap<>();

        List<String> data = Arrays.asList("aaaaa", "bbbbb", "ccccc", "ddddd");
        data.forEach(e -> {
            long crc64Id = CRC64Util.crc64Long(e);
            long num = crc64Id / Integer.MAX_VALUE;
            System.out.println(crc64Id + " -> " + num);
                // Positive 正
                Roaring64Bitmap rbMapOrDefault = positiveBitmapConcurrentHashMap.getOrDefault(num, new Roaring64Bitmap());
                rbMapOrDefault.addLong(crc64Id);
                positiveBitmapConcurrentHashMap.put(num, rbMapOrDefault);

        });

        // Positive 正
        positiveBitmapConcurrentHashMap.forEach((k, v) -> {
            System.out.println(k + " -> " + v);
            System.out.println("-----------------");
            System.out.println("LongSize: " + v.getLongSizeInBytes() + " SizeInBytes:" + v.getSizeInBytes());
            System.out.println("=================");
        });
    }

}
