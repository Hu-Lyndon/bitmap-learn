package org.example.test;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import org.example.util.CRC32Util;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/23 14:33
 */
public class HashTest {

    @Test
    public void testMurmurHash() {
        HashCode hexHashString = getHexHashString("123");
        System.out.println(hexHashString.asLong());

    }


    @Test
    public void testMurmurHashInt() {
        HashCode hexHashString = getHexHashString("123");
        System.out.println(hexHashString.asInt());

    }

    public static HashCode getHexHashString(String str) {
        HashFunction hashFunction = Hashing.murmur3_128();
        return hashFunction.hashString(str, StandardCharsets.UTF_8);
    }

    @Test
    public void testCrc32() {
        long l = CRC32Util.crc32Id("123");
        System.out.println(l);
    }

}
