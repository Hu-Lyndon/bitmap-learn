package org.example.test;

import org.example.set.Bitmap;
import org.example.util.CRC32Util;
import org.example.util.CRC64Util;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 9:28
 */
public class BitmapTest {

    @Test
    public void testBitmap() {

        List<String> data = Arrays.asList("aaaaa", "bbbbb");
        Bitmap bitmap = new Bitmap(Integer.MAX_VALUE, Integer.MAX_VALUE);
        System.out.println(bitmap.add(2L));

        for (String datum : data) {
            Long crcId = CRC32Util.crc32Id(datum);
            System.out.println("crcId:" + crcId);
            bitmap.add(crcId);
        }


        System.out.println(bitmap);
        System.out.println("size: " + bitmap.getSize() + " maxSize:" + bitmap.getMaxSize()
                + " stateNum:" + bitmap.getStateNum());
    }

    @Test
    public void testCRC64() {
        List<String> data = Arrays.asList("aaaaa", "bbbbb", "ccccc", "ddddd");
        data.forEach(e -> System.out.println(CRC64Util.crc64Long(e)));
        System.out.println("============================================");
        data.forEach(e -> System.out.println(Math.abs(CRC64Util.crc64Long(e))));
    }


    @Test
    public void testCRC64DivIntegerRange() {
        List<String> data = Arrays.asList("aaaaa", "bbbbb", "ccccc", "ddddd");
        data.forEach(e -> {
            long crc64Id = CRC64Util.crc64Long(e);
            long num = crc64Id / Integer.MAX_VALUE;
            long pos = crc64Id % Integer.MAX_VALUE;
            System.out.println(crc64Id + " -> " +num + " -> " + pos);
        });
    }

}
