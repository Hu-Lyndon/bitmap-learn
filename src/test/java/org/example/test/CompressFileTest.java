package org.example.test;

import org.example.util.Bz2Util;
import org.example.util.ZipUtil;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 13:17
 */
public class CompressFileTest {

    @Test
    public void testCompressFileBz2() throws IOException {
        File inputFile = new File("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
        String outputFile = "D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.tar.bz2";
        Bz2Util.compressToBz2(inputFile, outputFile, false);
    }



    @Test
    public void testCompressFileTarGz() throws IOException {
        File inputFile = new File("D:\\iProject\\javapath\\bitmap17\\data\\out\\crc32file.txt");
        String outputFile = "D:\\iProject\\javapath\\bitmap17\\data\\out";
        ZipUtil.compress(Arrays.asList(inputFile), outputFile, "crc32file.tar.gz");
    }

}
