package org.example.test;

import com.googlecode.javaewah.EWAHCompressedBitmap;
import org.example.util.CRC32Util;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/22 10:07
 */
public class JavaEWAHTest {
    @Test
    public void testJavaEWAH(){


        System.out.println(1 << 30);
        System.out.println(1 << 29);

        System.out.println(1 << 29 > 1 << 30);

        System.out.println("============================");

        EWAHCompressedBitmap ewahBitmap1 = EWAHCompressedBitmap.bitmapOf(0, 2, 55, 64, 1 << 30);
        EWAHCompressedBitmap ewahBitmap2 = EWAHCompressedBitmap.bitmapOf(1, 3, 64,1 << 30);
        //bitmap 1: {0,2,55,64,1073741824}
        System.out.println("bitmap 1: " + ewahBitmap1);
        //bitmap 2: {1,3,64,1073741824}
        System.out.println("bitmap 2: " + ewahBitmap2);

        //是否包含value=64，返回为true
        System.out.println(ewahBitmap1.get(64));

        //获取value的个数，个数为5
        System.out.println(ewahBitmap1.cardinality());

        //遍历所有value
        ewahBitmap1.forEach(integer -> {
            System.out.println(integer);
        });


        //进行位或运算
        EWAHCompressedBitmap orbitmap = ewahBitmap1.or(ewahBitmap2);
        //返回bitmap 1 OR bitmap 2: {0,1,2,3,55,64,1073741824}
        System.out.println("bitmap 1 OR bitmap 2: " + orbitmap);
        //memory usage: 40 bytes
        System.out.println("memory usage: " + orbitmap.sizeInBytes() + " bytes");

        //进行位与运算
        EWAHCompressedBitmap andbitmap = ewahBitmap1.and(ewahBitmap2);
        //返回bitmap 1 AND bitmap 2: {64,1073741824}
        System.out.println("bitmap 1 AND bitmap 2: " + andbitmap);
        //memory usage: 32 bytes
        System.out.println("memory usage: " + andbitmap.sizeInBytes() + " bytes");

        //序列化与反序列化
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ewahBitmap1.serialize(new DataOutputStream(bos));
            EWAHCompressedBitmap ewahBitmap1new = new EWAHCompressedBitmap();
            byte[] bout = bos.toByteArray();
            ewahBitmap1new.deserialize(new DataInputStream(new ByteArrayInputStream(bout)));
            System.out.println("bitmap 1 (recovered) : " + ewahBitmap1new);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testCase1() {
        // [435704073, 4237244439]
        List<String> data1 = Arrays.asList("aaaaa", "bbbbb");

        int CONTAINER_MAX = 31250000;
//        EWAHCompressedBitmap bm1 = new EWAHCompressedBitmap(CONTAINER_MAX);

        int a = 1 << 30;
        int b = 1;

        System.out.println("a :" + a + " b:" + b);

        EWAHCompressedBitmap bm1 = new EWAHCompressedBitmap();
//        bm1.set(a);
        bm1.set(b);
//        for (String datum1 : data1) {
//            long crcId = CRC32Util.crc32Id(datum1);
//            System.out.println("crcId:" + crcId);
////            bm1.addLiteralWord(crcId);
//            bm1.add(2 << 29);
//
////            System.out.println(bm1.get(crcId));
//        }

        // 0xffffffffL MAX_VALUE = 0x7fffffff;
        //Integer.MAX_VALUE
        // Long.MAX_VALUE;  0x7fffffffffffffffL

        System.out.println("bitmap 1: " + bm1);

        System.out.println(bm1.sizeInBytes());


        bm1.set(2);
        System.out.println("bitmap 1: " + bm1);

        System.out.println(bm1.sizeInBytes());


        bm1.set(a);
        System.out.println("bitmap 1: " + bm1);

        System.out.println(bm1.sizeInBytes());


//        List<Long> dataCrc32Ids = data1.stream().map(e -> CRC32Util.crc32Id(e)).collect(Collectors.toList());
//        System.out.println(dataCrc32Ids);

        //EWAHCompressedBitmap integers = EWAHCompressedBitmap.bitmapOf(435704073, 4237244439);


        System.out.println(bm1.getFirstSetBit());
    }


    @Test
    public void testCase2() {

        int CONTAINER_MAX = 31250000;
        EWAHCompressedBitmap bm1 = new EWAHCompressedBitmap(CONTAINER_MAX);
        bm1.set(435704073);
        //是否包含value=435704073，返回为true
        System.out.println(bm1.get(435704073));

        List<Integer> list = bm1.toList();
        System.out.println(list);
        System.out.println(list.size());

    }


}
