package org.example.test;

import org.junit.Test;
import org.xerial.snappy.Snappy;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * {@code @projectName} bitmap17
 * {@code @Author} huzhengyang
 * {@code @Create} 2024/4/26 12:50
 */
public class SnappyTest {

    @Test
    public void testSnappy1() throws IOException {
        String input = "Hello snappy-java! Snappy-java is a JNI-based wrapper of Snappy, a fast compresser/decompresser.";
        System.out.println("origin length: " + input.length());
        {
            byte[] compressed = Snappy.compress(input.getBytes("UTF-8"));
            System.out.println(new String(compressed, "UTF-8"));
            System.out.println("compressed 1 length: " + compressed.length);
            byte[] uncompressed = Snappy.uncompress(compressed);
            String result = new String(uncompressed, "UTF-8");
            System.out.println(result);
        }

        {
            byte[] compressed = Snappy.compress(input);
            System.out.println(new String(compressed, "UTF-8"));
            System.out.println("compressed 2 length: " + compressed.length);
            System.out.println(Snappy.uncompressString(compressed));
        }

        {
            double [] arr = new double[]{123.456,234.567,345.678};
            System.out.println("origin length: " + arr.length);
            byte[] compressed = Snappy.compress(arr);
            System.out.println(new String(compressed, "UTF-8"));
            System.out.println("compressed length: " + compressed.length);
            double [] unarr = Snappy.uncompressDoubleArray(compressed);
            System.out.println(Arrays.toString(unarr));
        }
    }

    @Test
    public void testSnappy2() throws IOException {
        byte[] compressed = Snappy.compress("aaaaa1".getBytes("UTF-8"));
        System.out.println(compressed);
    }

}
